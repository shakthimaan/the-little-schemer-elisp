;;
;; Code examples for Emacs Lisp for the book, "The Little Schemer" by
;; Daniel P. Friedman and Matthias Felleisen, published by The MIT
;; Press

;;
;; Chapter 1: Toys
;;

;; atom
;; Sexp -> Bool
;; Returns true if the argument is an atom.
(defun atom? (x)
  (not (listp x)))

(atom? 'Harry) ; t
(atom? '(1 2 3)) ; nil

;;
;; Chapter 2: Do It, Do It Again, and Again, and Again ...
;;

;; lat 
;; Sexp -> Bool
;; Checks if the argument passed is a list of atoms.
(defun lat? (x)
  (and (listp x) (every (function atom?) x)))

(lat? '(soured milk)) ; t
(lat? '((Jack) Sprat could eat no chicken fat)) ; nil

;; else in Scheme is t in Lisp
;; List -> Bool
;; Takes a list and returns true if it is a list of atoms.
(defun lats? (list)
  (cond
   ((null list) t)
   ((atom? (car list)) (lat? (cdr list)))
   (t nil)))

(lats? '(1 2 3 4 5)) ; t
(lats? '(1 (2 3) 4 5)) ; nil

;;
;; Chapter 3: Cons the Magnificent
;;

;; rember
;; Atom -> LAT -> LAT
;; Takes a member and a list and returns the list with the member
;; removed.
(defun rember (a list)
  (cond
   ((null list) '())
   ((eq (car list) a)
    (cdr list))
   (t (cons (car list) (rember a (cdr list))))))

(rember 2 '(1 2 3 4 5)) ; (1 3 4 5)

;; firsts
;; List -> List
;; Takes a list containing a null or non-empty lists and returns the
;; first element of each internal list.
(defun firsts (list)
  (cond
   ((null list) '())
   (t (cons (car (car list)) 
	    (firsts (cdr list))))))

(firsts '((1 2) (3 4))) ; (1 3)

;; insertR
;; New -> Old -> List
;; Appends the new element to the right of the old element for a list.
(defun insertR (new old list)
  (cond
   ((null list) '())
   ((eq (car list) old) 
    (cons old (cons new (cdr list))))
   (t (cons (car list) 
	    (insertR new old (cdr list))))))

(insertR 'topping 'fudge '(ice cream with fudge for desert)) ; (ice cream with fudge topping for desert)

;; insertL
;; New -> Old -> List
;; Appends the new element to the left of the first occurrence of an old element for a list.
(defun insertL (new old list)
  (cond
   ((null list) '())
   ((eq (car list) old) (cons new (cons old (cdr list))))
   (t (cons (car list) (insertL new old (cdr list))))))

(insertL 'topping 'fudge '(ice cream with fudge for desert)) ; (ice cream with topping fudge for desert)
(insertL 'topping 'fudge '(ice cream with fudge for desert fudge)) ; (ice cream with topping fudge for desert fudge)

;; subst
;; New -> Old -> List
;; Takes a new and an old element and substitutes the old element with
;; the new to a new list.
(defun subst (new old list)
  (cond
   ((null list) '())
   ((eq (car list) old) (cons new (cdr list)))
   (t (cons (car list) (subst new old (cdr list))))))

(subst 'topping 'fudge '(ice cream with fudge for desert)) ; (ice cream with topping for desert)

;; subst2
;; New -> Old1 -> Old2 -> List
;; Takes a new element and two old elements and replaces the first
;; occurrence of either of the old elements with the new element.
(defun subst2 (new old1 old2 list)
  (cond
   ((null list) '())
   ((or (eq (car list) old1) (eq (car list) old2))
       (cons new (cdr list)))
   (t cons (car list) (subst2 new old1 old2 (cdr list)))))

(subst2 'vanilla 'chocolate 'banana '(banana ice cream with chocolate topping)) ; (vanilla ice cream with chocolate topping)

;; multirember
;; Atom -> List -> List
;; Removes all instances of an atom in a list.
(defun multirember (a list)
  (cond
   ((null list) '())
   ((eq (car list) a)
    (multirember a (cdr list)))
   (t (cons (car list) (multirember a (cdr list))))))

(multirember 2 '(1 2 3 4 2 5)) ; (1 3 4 5)

;; multiinsertL
;; New -> Old -> List -> List
;; Takes a new element, an old element, a list, and inserts the new
;; element to the left of each and every occurrence of the old element.
(defun multiinsertL (new old list)
  (cond
   ((null list) '())
   ((eq (car list) old) (cons new (cons old (multiinsertL new old (cdr list)))))
   (t (cons (car list) (multiinsertL new old (cdr list))))))

(multiinsertL 'fried 'fish '(chips and fish or fish and fried)) ; (chips and fried fish or fried fish and fried)

;; multisubst
;; New -> Old -> List -> List
;; Takes a new element, an old element, a list and returns a list with
;; all the old elements replaced by the new element.
(defun multisubst (new old list)
  (cond
   ((null list) '())
   ((eq (car list) old) (cons new (multisubst new old (cdr list))))
   (t (cons (car list) (multisubst new old (cdr list))))))

(multisubst 'vegetable 'fish '(chips and fish or fish and fried)) ; (chips and vegetable or vegetable and fried)

;;
;; Chapter 4: Numbers Games
;;

;; add1
;; Number -> Number
;; Increments the number argument by one.
(defun add1 (n)
  (+ n 1))

(add1 67) ; 68

;; sub1
;; Number -> Number
;; Subtracts one from the number argument.
(defun sub1 (n)
  (- n 1))

(sub1 67) ; 66
(sub1 0)  ; -1

(zerop 0) ; t

;; pplus
;; Number -> Number -> Number
;; Takes two numbers and returns their sum.
(defun pplus (n m)
  (cond
   ((zerop m) n)
   (t (add1 (pplus n (sub1 m))))))

(pplus 2 3) ; 5

;; subminus
;; Number -> Number -> Number
;; Takes two numbers and returns the difference of the second from the
;; first.
(defun subminus (n m)
  (cond
   ((zerop m) n)
   (t (sub1 (subminus n (sub1 m))))))

(subminus 2 3) ; -1

;; addtup
;; Tuple -> Number
;; Takes a tuple of numbers and returs their sum
(defun addtup (tup)
  (cond
   ((null tup) 0)
   (t (+ (car tup) (addtup (cdr tup))))))

(addtup '(1 2 3 4 5)) ; 15

;; mult
;; Number -> Number -> Number
;; Takes two numbers and returns the product
(defun mult (n m)
  (cond
   ((zerop m) 0)
   (t (+ n (mult n (sub1 m))))))

(mult 4 3) ; 12

;; tupplus
;; Tuple -> Tuple -> List
;; Takes two tuples and returns a list with the sum of the respective
;; members.
(defun tupplus (tup1 tup2)
  (cond
   ((null tup1) tup2)
   ((null tup2) tup1)
   (t (cons (+ (car tup1) (car tup2)) (tupplus (cdr tup1) (cdr tup2))))))

(tupplus '(1 2) '(3 4)) ; (4 6)

(tupplus '(1 2 3) '(1)) ; (2 2 3)
(tupplus '(1 2) '(2 3 4 5)) ; (3 5 4 5)

;; greaterthan
;; Number -> Number -> Bool
;; Takes two numbers and returns true if the first is greater than the
;; second. Otherwise, returns nil.
(defun greaterthan (n m)
  (cond
   ((zerop m) t)
   ((zerop n) nil)
   (t (greaterthan (sub1 n) (sub1 m)))))

(greaterthan 12 133) ; nil
(greaterthan 120 11) ; t

;; lessthan
;; Number -> Number -> Bool
;; Takes two numbers and returns true if the first is less than the
;; second. Otherwise, returns nil.
(defun lessthan (n m)
  (cond
   ((zerop m) nil)
   ((zerop n) t)
   (t (lessthan (sub1 n) (sub1 m)))))

(lessthan 12 133) ; t
(lessthan 120 11) ; nil

; equalto
;; Number -> Number -> Bool
;; Takes two numbers and returns true if both are equal. Otherwise,
;; returns nil.
(defun equalto (n m)
  (cond
   ((zerop m) (zerop n))
   ((zerop n) nil)
   (t (equalto (sub1 n) (sub1 m)))))

(equalto 12 133) ; nil
(equalto 120 11) ; nil
(equalto 12 12)  ; t

;; equaltothan
;; Number -> Number -> Bool
;; Takes two numbers and returns true if both are equal. Otherwise,
;; returns nil.
;; Rewrite equal to with < and >.
(defun equaltothan (n m)
  (cond
   ((> n m) nil)
   ((< n m) nil)
   (t t)))

(equaltothan 12 133) ; nil
(equaltothan 120 11) ; nil
(equaltothan 12 12)  ; t

;; exponent
;; Number -> Number -> Number
;; Takes a base and a power and returns the exponent.
(defun exponent (n m)
  (cond
   ((zerop m) 1)
   (t (* n (exponent n (sub1 m))))))

(exponent 1 1) ; 1
(exponent 2 3) ; 8
(exponent 5 3) ; 125

;; division
;; Number -> Number -> Number
;; Takes a number and a divisor and returns the quotient.
(defun divide (n m)
  (cond
   ((lessthan n m) 0)
   (t (add1 (divide (- n m) m)))))

(divide 15 4) ; 3

;; length lat
;; LAT -> Number
;; Takes a LAT and returns its count
(defun length (lat)
  (cond
   ((null lat) 0)
   (t (add1 (length (cdr lat))))))

(length '(ham and cheese on rye)) ; 5

;; pick n lat
;; Number -> LAT -> Atom
;; Takes the nth element from a LAT
(defun pick (n lat)
  (cond
   ((zerop (sub1 n)) (car lat))
   (t (pick (sub1 n) (cdr lat)))))

(pick 4 '(lasagna spaghetti ravioli macaroni meatball)) ; macaroni

;; rempick
;; Number -> LAT -> List
;; Removes the nth element from a LAT and returns the remaining list.
(defun rempick (n lat)
  (cond
   ((zerop (sub1 n)) (cdr lat))
   (t (cons (car lat)
	    (rempick (sub1 n) (cdr lat))))))

(rempick 3 '(hotdogs with hot mustard)) ; (hotdogs with mustard)

;; no-nums
;; LAT -> List
;; Removes the numbers from a LAT and returns the remaining list.
(defun no-nums (lat)
  (cond
   ((null lat) '())
   (t (cond
       ((numberp (car lat))
	(no-nums (cdr lat)))
       (t (cons (car lat)
		(no-nums (cdr lat))))))))

(no-nums '(5 pears 6 prunes 9 dates)) ; (pears prunes dates)

;; all-nums
;; LAT -> List
;; Takes a LAT as an argument and returns the list with only numbers.
(defun all-nums (lat)
  (cond
   ((null lat) '())
   (t (cond
       ((numberp (car lat))
	(cons (car lat)
	      (all-nums (cdr lat))))
       (t (all-nums (cdr lat)))))))

(all-nums '(5 pears 6 prunes 9 dates)) ; (5 6 9)

;; eqan
;; Atom -> Atom -> Bool
;; Takes two atoms and returns true if the atoms are equal.
(defun eqan (a1 a2)
  (cond
   ((and (numberp a1) (numberp a2))
    (= a1 a2))
   ((or (numberp a1) (numberp a2))
    nil)
   (t (eq a1 a2))))

(eqan 2 3) ; nil
(eqan 2 2) ; t
(eqan 2 '(2 3)) ; nil

;; occur
;; Atom -> LAT -> Number
;; Takes an atom and a LAT, and returns the number of times the atom
;; occurs in the LAT.
(defun occur (a lat)
  (cond
   ((null lat) 0)
   (t (cond
       ((eq (car lat) a)
	(add1 (occur a (cdr lat))))
       (t (occur a (cdr lat)))))))

(occur 2 '(1 2 3 4 2 3 4 2)) ; 3

;; one?
;; Number -> Bool
;; Takes a number and returns true if it is a one.
(defun one (n)
  (= n 1))

(one 2) ; nil
(one 1) ; t

;; rempick with one?
;; Number -> LAT -> List
;; Removes the nth element from a LAT and returns a list.
(defun rempickone (n lat)
  (cond
   ((one n) (cdr lat))
   (t (cons (car lat)
	    (rempick (sub1 n) (cdr lat))))))

(rempickone 3 '(lemon meringue salty pie)) ; (lemon meringue pie)

;;
;; Chapter 5: *Oh My Gawd*: It's Full of Stars
;;

;; rember*
;; Atom -> List -> List
;; Removes all occurrences of an atom recursively in an Sexp.
(defun rember* (a list)
  (cond
   ((null list) '())
   ((atom? (car list))
    (cond
     ((eq (car list) a)
      (rember* a (cdr list)))
     (t (cons (car list) (rember* a (cdr list))))))
   (t (cons (rember* a (car list)) (rember* a (cdr list))))))

(rember* 'sauce '((((tomato sauce)) ((bean) sauce) (and ((flying)) sauce)))) 
; ((((tomato)) ((bean)) (and ((flying))))

;; insertR*
;; New -> Old -> List
;; Inserts a new member to the right of an old member recursively in an Sexp.
(defun insertR* (new old list)
  (cond
   ((null list) '())
   ((atom? (car list))
    (cond
     ((eq (car list) old)
      (cons old
	    (cons new
		  (insertR* new old (cdr list)))))
     (t (cons (car list) (insertR* new old (cdr list))))))
   (t (cons (insertR* new old (car list))
	    (insertR* new old (cdr list))))))

(insertR* 'roast 'chuck '(((how much (wood)) 
			   could 
			   ((a (wood) chuck))
			   (((chuck)))
			   (if(a)((wood chuck)))
			   could chuck wood)))

; ((how much (wood)) 
;   could 
;   ((a (wood) chuck roast))
;   (((chuck roast)))
;   (if(a)((wood chuck roast)))
;   could chuck roast wood))

;; occur*
;; Atom -> List -> Number
;; Counts the number of times an atom occurs in a list recursively.
(defun occur* (a list)
  (cond
   ((null list) 0)
   ((atom? (car list))
    (cond
     ((eq (car list) a)
      (add1 (occur* a (cdr list))))
     (t (occur* a (cdr list)))))
   (t (+ (occur* a (car list)) (occur* a (cdr list))))))

(occur* 'banana '(((banana)
		   (split ((((banana ice)))
			   (cream (banana))
			   sherbet))
		   (banana)
		   (bread)
		   (banana brandy)))) ; 5

;; subst*
;; New -> Old -> List -> List
;; Takes a new element and replaces all occurences of an old element
;; recursively in a list.
(defun subst* (new old list)
  (cond
   ((null list) '())
   ((atom? (car list))
    (cond
     ((eq (car list) old)
      (cons new (subst* new old (cdr list))))
     (t (cons (car list) (subst* new old (cdr list))))))
   (t (cons (subst* new old (car list)) 
	    (subst* new old (cdr list))))))
	   
(subst* 'orange 'banana '(((banana)
			   (split ((((banana ice)))
				   (cream (banana))
				   sherbet))
			   (banana)
			   (bread)
			   (banana brandy))))

; ((orange)
;  (split ((((orange ice)))
;          (cream (orange))
;	   sherbet))
;   (orange)
;   (bread)
;   (orange brandy)))

;; insertL*
;; New -> Old -> List -> List
;; Inserts a new element to the left of an old element recursively in
;; a list and returns the list.
(defun insertL* (new old list)
  (cond
   ((null list) '())
   ((atom? (car list))
    (cond
     ((eq (car list) old)
      (cons new
	    (cons old
		  (insertL* new old
			    (cdr list)))))
     (t (cons (car list)
	      (insertL* new old
			(cdr list))))))
   (t (cons (insertL* new old (car list))
	    (insertL* new old (cdr list))))))

(insertL* 'pecker 'chuck '(((how much (wood)) 
			    could 
			    ((a (wood) chuck))
			    (((chuck)))
			    (if(a)((wood chuck)))
			    could chuck wood)))

; ((how much (wood)) 
;  could 
;  ((a (wood) pecker chuck))
;  (((pecker chuck)))
;  (if(a)((wood pecker chuck)))
;  could pecker chuck wood))

;; member*
;; Atom -> List -> Bool
;; Takes an atom and a list and checks if the atom is present in the
;; list recursively.
(defun member* (a list)
  (cond
   ((null list) nil)
   ((atom? (car list))
    (or (eq (car list) a)
	(member* a (cdr list))))
   (t (or (member* a (car list))
	  (member* a (cdr list))))))

(member* 'chips '(((potato) (chips ((with) fish) (chips))))) ; t
(member* 'carrot '(((potato) (chips ((with) fish) (chips))))) ; nil

;; leftmost
;; List -> Atom
;; Returns the leftmost atom in a non-empty list of Sexp.
(defun leftmost (list)
  (cond
   ((atom? (car list)) (car list))
   (t (leftmost (car list)))))

(leftmost '(((potato) (chips ((with) fish) (chips))))) ; potato

(leftmost '((((hot) (tuna (and))) cheese))) ; hot

(leftmost '(())) ; No answer

(leftmost '((((() four)) 17 (seventeen)))) ; No answer

;; Expressed as abbreviations of (cond ...)-expressions:
;; (and a b) = (cond (a b) (else nil))
;; (or a b)  = (cond (a t) (else b))

;; eqlist?
;; List -> List -> Bool
;; Takes two lists and determines if they are equal.
(defun eqlist? (list1 list2)
  (cond
   ((and (null list1) (null list2)) t)
   ((and (null list1) (atom? (car list2))) 
    nil)
   ((null list1) nil)
   ((and (atom? (car list1)) (null list2))
    nil)
   ((and (atom? (car list1))
	 (atom? (car list2)))
    (and (eqan (car list1) (car list2))
	 (eqlist? (cdr list1) (cdr list2))))
   ((atom? (car list1)) nil)
   ((null list2) nil)
   ((atom? (car list2)) nil)
   (t
    (and (eqlist? (car list1) (car list2))
	 (eqlist? (cdr list1) (cdr list2))))))

(eqlist? '(strawberry ice cream) '(strawberry ice cream)) ; t

(eqlist? '(strawberry ice cream) '(strawberry cream ice)) ; nil

(eqlist? '(banana ((split))) '(((banana) (split)))) ; nil

(eqlist? '((beef ((sausage)) (and (soda)))) '((beef ((salami)) (and (soda))))) ; nil

(eqlist? '((beef ((sausage)) (and (soda)))) '((beef ((sausage)) (and (soda))))) ; t

;; eqlist-simplify?
;; List -> List -> Bool
;; Takes two lists and determines if they are equal.
;; simplify eqlist?
(defun eqlist-simplify? (list1 list2)
  (cond
   ((and (null list1) (null list2)) t)
   ((or (null list1) (null list2)) nil)
   ((and (atom? (car list1))
	 (atom? (car list2)))
    (and (eqan (car list1) (car list2))
	 (eqlist-simplify? (cdr list1) (cdr list2))))
   ((or (atom? (car list1)) (atom? (car list2))) nil)
   (t
    (and (eqlist-simplify? (car list1) (car list2))
	 (eqlist-simplify? (cdr list1) (cdr list2))))))

(eqlist-simplify? '(strawberry ice cream) '(strawberry ice cream)) ; t

(eqlist-simplify? '(strawberry ice cream) '(strawberry cream ice)) ; nil

(eqlist-simplify? '(banana ((split))) '(((banana) (split)))) ; nil

(eqlist-simplify? '((beef ((sausage)) (and (soda)))) '((beef ((salami)) (and (soda))))) ; nil

(eqlist-simplify? '((beef ((sausage)) (and (soda)))) '((beef ((sausage)) (and (soda))))) ; t

;; equal?
;; Sexp -> Sexp -> Bool
;; Takes two Sexp and returns true if they are the same.
(defun equal? (s1 s2)
  (cond
   ((and (atom? s1) (atom? s2))
    (eqan s1 s2))
   ((atom? s1) nil)
   ((atom? s2) nil)
   (t (eqlist? s1 s2))))

(equal? '1 '2) ; nil
(equal? '1 '1) ; t

(equal? '(one) '2) ; nil

(equal? '2 '(two)) ; nil

(equal? '(one) '(two)) ; nil

(equal? '(one two) '(one two)) ; t

;; equal-simplify?
;; Sexp -> Sexp -> Bool
;; Takes two Sexp and returns true if they are the same.
;; simplify equal?
(defun equal-simplify? (s1 s2)
  (cond
   ((and (atom? s1) (atom? s2))
    (eqan s1 s2))
   ((or (atom? s1) (atom? s2))
    nil)
   (t (eqlist? s1 s2))))

(equal-simplify? '1 '2) ; nil
(equal-simplify? '1 '1) ; t

(equal-simplify? '(one) '2) ; nil

(equal-simplify? '2 '(two)) ; nil

(equal-simplify? '(one) '(two)) ; nil

(equal-simplify? '(one two) '(one two)) ; t

;; eqlist-equal?
;; List -> List -> Bool
;; Takes two lists and returns true if they are equal.
;; write eqlist? using equal?
(defun eqlist-equal? (list1 list2)
  (cond
   ((and (null list1) (null list2)) t)
   ((or (null list1) (null list2)) nil)
   (t
    (and (equal? (car list1) (car list2))
	 (eqlist-equal? (cdr list1) (cdr list2))))))
  
(eqlist-equal? '(one) '(two)) ; nil

(eqlist-equal? '(one two) '(one two)) ; t

(eqlist-equal? '(four (two three)) '(four (two five))) ; nil

;;
;; Chapter 6: Shadows
;;

(equal? 'a 'a)

;; numbered
;; Aexp -> Bool
;; Takes an arithmetic expression and returns true if it contains only
;; numbers.
(defun numbered? (aexp)
  (cond
   ((atom? aexp) (numberp aexp))
   (t
    (and (numbered? (car aexp))
	 (numbered?
	  (car (cdr (cdr aexp))))))))

(numbered? 3) ; t
(numbered? '(3 + 4)) ; t
(numbered? '(2 x 'message)) ; nil

;; 1st-sub-exp
;; Aexp -> Number
;; Returns the first sub-expression in an arithmetic expression.
(defun 1st-sub-exp (aexp)
  (car (cdr aexp)))         ; 1 in (+ 1 3)

(1st-sub-exp '(+ 1 3)) ; 1

;; 2nd-sub-exp
;; Aexp -> Number
;; Returns the second sub-expression in an arithmetic expression.
(defun 2nd-sub-exp (aexp)
  (car (cdr (cdr aexp))))   ; 3 in (+ 1 3)

(2nd-sub-exp '(+ 1 3)) ; 3

;; operator
;; Aexp -> Operator
;; Returns the operator an arithmetic expression.
(defun operator (aexp)
  (car aexp))               ; + in (+ 1 3)

(operator '(+ 1 3)) ; +

;; value
;; Nexp -> Number
;; Returns the value of the numbered arithmetic expression.
(defun value (nexp)
  (cond
   ((atom? nexp) nexp)
   ((equal? (operator nexp) '+)
    (+ (value (1st-sub-exp nexp))
       (value (2nd-sub-exp nexp))))
   ((equal? (operator nexp) '*)
    (* (value (1st-sub-exp nexp))
       (value (2nd-sub-exp nexp))))
   (t
    (expt (value (1st-sub-exp nexp))
	  (value (2nd-sub-exp nexp))))))

(value '2) ; 2
(value '+) ; +
(value '(+ 2 3)) ; 5

;; sero?
;; Sero -> Bool
;; Test for zero representation.
(defun sero? (n)
  (null n))

(sero? '()) ; t
(sero? '(() () ())) ; nil

;; edd1
;; Sero -> Sero
;; Add one to a zero representation.
(defun edd1 (n)
  (cons '() n))

(edd1 '(() ())) ; (nil nil nil)

;; zub1
;; Sero -> Sero
;; Subtract one from a zero representation.
(defun zub1 (n)
  (cdr n))

(zub1 '(() ())) ; (nil)

(zub1 '(() () ())) ; (nil nil)

;; zero-plus
;; Sero -> Sero -> Sero
;; Add two zero representations.
(defun zero-plus (n m)
  (cond
   ((sero? m) n)
   (t (edd1 (zero-plus n (zub1 m))))))

(zero-plus '(()) '()) ; (nil)

(zero-plus '(() ()) '()) ; (nil nil)

(zero-plus '(() ()) '(() ())) ; (nil nil nil nil)

;; zero-lat
;; List -> Bool
;; Checks if the list contains a valid zero representation.
(defun zero-lat? (list)
  (cond
   ((sero? list) t)
   ((atom? (car list)) (zero-lat? (cdr list)))
   (t nil)))

(zero-lat? '()) ; t

(zero-lat? '(() ())) ; nil

;;
;; Chapter 7: Friends and relations
;;

;; set?
;; LAT -> Bool
;; Takes a LAT and returns true if it is a set, false otherwise.
(defun set? (lat)
  (cond
   ((null lat) t)
   ((member* (car lat) (cdr lat)) nil)
   (t (set? (cdr lat)))))

(set? '(apple peaches apple plum)) ; nil

(set? '(apples peaches pears plums)) ; t

;; makeset
;; LAT -> List
;; Takes a LAT and returns a set from the elements in the LAT.
(defun makeset (lat)
  (cond
   ((null lat) '())
   ((member* (car lat) (cdr lat))
    (makeset (cdr lat)))
   (t (cons (car lat) (makeset (cdr lat))))))

(makeset '(apple peach pear peach plum apple lemon peach)) ; (pear plum apple lemon peach)

;; makeset-multirember
;; LAT -> List
;; Takes a LAT and returns a set from the elements in the LAT
;; makeset using multirember.
(defun makeset-multirember (lat)
  (cond
   ((null lat) '())
   (t (cons (car lat)
	    (makeset (multirember (car lat) (cdr lat)))))))

(makeset-multirember '(apple peach pear peach plum apple lemon peach)) ; (pear plum apple lemon peach)

;; subset?
;; Set -> Set -> Bool
;; Takes two sets and determines if each atom in the first set is also
;; present in the second.
(defun subset? (set1 set2)
  (cond
   ((null set1) t)
   (t (cond
       ((member (car set1) set2)
	(subset? (cdr set1) set2))
       (t nil)))))

(subset? '(5 chicken wings) '(5 hamburgers 2 pieces fried chicken and light duckling wings)) ; t

(subset? '(4 pounds of horseradish) '(four pounds chicken and 5 ounces horseradish)) ; nil

;; eqset?
;; Set -> Set -> Bool
;; Takes two sets and determines if they are equal
(defun eqset? (set1 set2)
  (cond
   ((subset? set1 set2)
    (subset? set2 set1))
   (t nil)))

(eqset? '(6 large chickens with wings) '(6 chickens with large wings)) ; t
(eqset? '(6 large chickens with wings) '(5 chickens with large wings)) ; nil

;; eqset-one-cond?
;; Set -> Set -> Bool
;; Takes two sets and determines if they are equal
;; Written with only one condition line.
(defun eqset-one-cond? (set1 set2)
  (cond
   (t (and (subset? set1 set2) (subset? set2 set1)))))

(eqset-one-cond? '(6 large chickens with wings) '(6 chickens with large wings)) ; t
(eqset-one-cond? '(6 large chickens with wings) '(5 chickens with large wings)) ; nil

;; eqset-one-cond?
;; Set -> Set -> Bool
;; Takes two sets and determines if they are equal
;; Written with only one condition line.
(defun eqset-one-liner? (set1 set2)
  (and (subset? set1 set2) (subset? set2 set1)))

(eqset-one-liner? '(6 large chickens with wings) '(6 chickens with large wings)) ; t
(eqset-one-liner? '(6 large chickens with wings) '(5 chickens with large wings)) ; nil

;; intersect
;; Set1 -> Set2 -> Bool
;; Takes two sets and returns true if there is at least one element
;; that is common, returns false, otherwise.
(defun intersect? (set1 set2)
  (cond
   ((null set1) nil)
   (t
    (cond
     ((member* (car set1) set2) t)
     (t
      (intersect? (cdr set1) set2))))))

(intersect? '(stewed tomatoes and macaroni) '(macaroni and cheese)) ; t
(intersect? '(stewed tomatoes and macaroni) '(vegetable cheese)) ; nil

;; intersect-short
;; Set1 -> Set2 -> Bool
;; Takes two sets and returns true if there is at least one element
;; that is common, returns false, otherwise.
(defun intersect-short? (set1 set2)
  (cond
   ((null set1) nil)
   ((member* (car set1) set2) t)
   (t (intersect-short? (cdr set1) set2))))

(intersect-short? '(stewed tomatoes and macaroni) '(macaroni and cheese)) ; t
(intersect-short? '(stewed tomatoes and macaroni) '(vegetable cheese)) ; nil

;; intersect-or?
;; Set1 -> Set2 -> Bool
;; Takes two sets and returns true if there is at least one element
;; that is common, returns false, otherwise.
(defun intersect-or? (set1 set2)
  (cond
   ((null set1) nil)
   (t (or (member* (car set1) set2) (intersect-or? (cdr set1) set2)))))

(intersect-or? '(stewed tomatoes and macaroni) '(macaroni and cheese)) ; t
(intersect-or? '(stewed tomatoes and macaroni) '(vegetable cheese)) ; nil

;; intersect
;; Set1 -> Set2 -> List
;; Takes two sets and returns a list with elements common in both the
;; sets.
(defun intersect (set1 set2)
  (cond
   ((null set1) '())
   ((member* (car set1) set2)
    (cons (car set1)
	  (intersect (cdr set1) set2)))
   (t (intersect (cdr set1) set2))))

(intersect '(stewed tomatoes and macaroni) '(macaroni and cheese)) ; (and macaroni)
(intersect '(stewed tomatoes and macaroni) '(vegetable cheese)) ; nil

;; union
;; Set1 -> Set2 -> List
;; Takes two sets and returns the union of two sets.
(defun union (set1 set2)
  (cond
   ((null set1) set2)
   ((member* (car set1) set2)
    (union (cdr set1) set2))
   (t (cons (car set1) (union (cdr set1) set2)))))

(union '(stewed tomatoes and macaroni casserole) '(macaroni and cheese)) ; (stewed tomatoes casserole macaroni and cheese)

;; set-difference
;; Set1 -> Set2 -> List
;; It takes two sets and returns all atoms in set1 that are not in
;; set2.
(defun set-difference (set1 set2)
  (cond
   ((null set1) '())
   ((member (car set1) set2)
    (set-difference (cdr set1) set2))
   (t
    (cons (car set1)
	  (set-difference (cdr set1) set2)))))

(set-difference '(alpha beta) '(alpha gamma delta)) ; (beta)

;; intersectall
;; LSet -> List
;; Takes a list of non-empty sets and returns a list with elements
;; present in all the lists.
(defun intersectall (lset)
  (cond
   ((null (cdr lset)) (car lset))
   (t (intersect (car lset)
		 (intersectall (cdr lset))))))

(intersectall '((6 pears and)
		(3 peaches and 6 peppers)
		(8 pears and 6 plums)
		(and 6 prunes with some apples))) ; (6 and)

;; a-pair?
;; List -> Bool
;; Takes a list of Sexp and returns true if it is a pair, false
;; otherwise.
(defun a-pair? (x)
  (cond
   ((atom? x) nil)
   ((null x) nil)
   ((null (cdr x)) nil)
   ((null (cdr (cdr x))) t)
   (t nil)))

(a-pair? '(pear pear)) ; t
(a-pair? '(3 7)) ; t
(a-pair? '((2) (pair))) ; t
 (a-pair? '(full (house))) ; t
(a-pair? '(2 3 4)) ; nil

;; first
;; Pair -> Sexp
;; Takes a pair and returns the first Sexp
(defun first (p)
  (cond
   (t (car p))))

(first '(2 3)) ; 2

;; second
;; Pair -> Sexp
;; Takes a pair and returns the second Sexp
(defun second (p)
  (cond
   (t (car (cdr p)))))

(second '(2 3)) ; 3

;; third
;; List -> Atom
;; Takes a list and returns the third element from the list
(defun third (l)
  (car (cdr (cdr l))))

(third '(1 2 3)) ; 3

;; build
;; Set1 -> Set2 -> List
;; Takes two sets and builds a new list
(defun build (s1 s2)
  (cond
   (t (cons s1 (cons s2 '())))))

(build '(1 2) '(3 4)) ; ((1 2) (3 4))
(build 1 2) ; (1 2)

;; fun?
;; Relation -> Bool
;; Takes a relation and checks if it is a relation or not.
(defun fun? (rel)
  (set? (firsts rel)))

(fun? '((4 3) (4 2))) ; nil
(fun? '((d 4) (b 0) (b 9))) ; nil
(fun? '((8 3) (4 2) (7 6) (6 2) (3 4))) ; t

;; revrel
;; Relation -> Relation
;; Takes a relation and produces its inverse relation.
(defun revrel (rel)
  (cond
   ((null rel) '())
   (t (cons (build
	     (second (car rel))
	     (first (car rel)))
	    (revrel (cdr rel))))))

(revrel '((8 a) (pumpkin pie) (got sick))) ; ((a 8) (pie pumpkin) (sick got))
	     
;; revpair
;; Pair -> List
;; Takes a pair and returns the list with the pair reversed
(defun revpair (pair)
  (build (second pair) (first pair)))

(revpair '(1 2)) ; (2 1)
(revpair '((1 2) (3 4))) ; (3 4) (1 2)

(defun revrel-revpair (rel)
  (cond
   ((null rel) '())
   (t (cons (revpair (car rel))
	    (revrel-revpair (cdr rel))))))

(revrel-revpair '((8 a) (pumpkin pie) (got sick))) ; ((a 8) (pie pumpkin) (sick got))

;; seconds
;; List -> List
;; Takes a list containing a null or non-empty lists and returns the
;; second element of each internal list.
(defun seconds (list)
  (cond
   ((null list) '())
   (t (cons (car (cdr (car list)))
	    (seconds (cdr list))))))

(seconds '((1 2) (3 4) (5 6))) ; ((2) (4) (6))

;; fullfun?
;; Function -> Bool
;; Takes a function and determines if it is a real function
(defun fullfun? (fun)
  (set? (seconds fun)))

(fullfun? '((8 3) (4 8) (7 6) (6 2) (3 4))) ; t

(fullfun? '((grape raisin) (plum prune) (stewed prune))) ; nil

;; one-to-one?
(defun one-to-one? (rel)
  (fun? (revrel rel)))

(one-to-one? '((chocolate chip) (doughy cookie))) ; t
(one-to-one? '((chocolate chip) (doughy chip))) ; nil

;;
;; Chapter 8: Lambda the Ultimate
;;

;; rember-f
;; Function -> Atom -> List -> List
;; Takes a function, an atom to apply the function to a list, and
;; returns a list without the members whose function application
;; matches.
(defun rember-f (test? a l)
  (cond
   ((null l) '())
   (t (cond
       ((funcall test? (car l) a) (cdr l))
       (t (cons (car l)
		(rember-f test? a (cdr l))))))))

(rember-f 'eqan 5 '(6 2 5 3)) ; (6 2 3)
(rember-f 'eq 'jelly '(jelly beans are good)) ; (beans are good)
(rember-f 'equal? '(pop corn) '(lemonade (pop corn) and (cake))) ; (lemonade and (cake))

;; rember-f-short
;; Function -> Atom -> List -> List
;; Takes a function, an atom to apply the function to a list, and
;; returns a list without the members whose function application
;; matches. This is a shorter version.
(defun rember-f-short (test? a l)
  (cond
   ((null l) '())
   ((funcall test? (car l) a) (cdr l))
   (t (cons (car l) 
	    (rember-f-short test? a (cdr l))))))

(rember-f-short 'eqan 5 '(6 2 5 3)) ; (6 2 3)
(rember-f-short 'eq 'jelly '(jelly beans are good)) ; (beans are good)
(rember-f-short 'equal? '(pop corn) '(lemonade (pop corn) and (cake))) ; (lemonade and (cake))

;; eq?-c
;; Argument -> Function
;; Takes an argument 'a' and returns a function that takes another
;; argument.
(eval
 '(defun eq?-c (a)
    (function
     (lambda (x)
       (eq x a))))
 t)
 
(eq?-c 'salad)

(funcall (eq?-c 'salad) 'salad) ; t
 
(apply (funcall 'eq?-c 'salad) '(salad)) ; t
(apply (funcall 'eq?-c 'salad) '(tuna)) ; nil

(setq eq?-salad (eq?-c 'salad)) ; (eq?-c (quote salad))

(funcall eq?-salad 'salad) ; t
(funcall eq?-salad 'tuna) ; nil

;; rember-f-lambda
;; Function -> Function
;; Takes a function and returns a function that accepts an atom and a
;; list, removes all occurrences of the atom in the list and returns a
;; new list.
(eval
 '(defun rember-f-lambda (test?)
    (function
     (lambda (a l)
       (cond
	((null l) '())
	((funcall test? (car l) a) (cdr l))
	(t (cons (car l) 
		 (funcall (rember-f-lambda test?) a (cdr l))))))))
 t)

(defun rember-eq? (rember-f-lambda eq))

(funcall rember-eq? 'tuna '(tuna salad is good)) ; (salad is good)

(funcall (rember-f-lambda 'eq) 'tuna '(tuna salad is good)) ; (salad is good)

(funcall (rember-f-lambda 'eq) 'tuna '(shrimp salad and tuna salad)) ; (shrimp salad and salad)

(rember-f 'eq 'eq? '(equal? eq? eqan? eqlist? eqpair?)) ; (equal? eqan? eqlist? eqpair?))

;; insertL-f-lambda
;; Function -> Function
;; Takes a function and returns a function that takes a new element,
;; on old element, and a list and prepends the first occurrence of the
;; old element with the new element and returns a new list.
(eval
 '(defun insertL-f-lambda (test?)
    (function
     (lambda (new old l)
       (cond
	((null l) '())
	((funcall test? (car l) old) (cons new (cons old (cdr l))))
	(t (cons (car l) 
		 (funcall (insertL-f-lambda test?) new old (cdr l))))))))
 t)

(funcall (insertL-f-lambda 'eq) 'teak 'wood '(how much wood could a wood chuck)) 
; (how much teak wood could a wood chuck)

;; insertR-f-lambda
;; Function -> Function
;; Takes a function and returns a function that takes a new element,
;; on old element, and a list and appends the first occurrence of the
;; old element with the new element and returns a new list.
(eval
 '(defun insertR-f-lambda (test?)
    (function
     (lambda (new old l)
       (cond
	((null l) '())
	((funcall test? (car l) old) (cons old (cons new (cdr l))))
	(t (cons (car l) 
		 (funcall (insertR-f-lambda test?) new old (cdr l))))))))
 t)

(funcall (insertR-f-lambda 'eq) 'log 'wood '(how much wood could a wood chuck))
; (how much wood log could a wood chuck)

;; seqL
;; New -> Old -> List
;; Takes a new element, an old element, a list, and constructs a new
;; list with the new element prepended to the old element.
(defun seqL (new old l)
  (cons new (cons old l)))

(seqL 'alpha 'beta '(gamma delta)) ; (alpha beta gamma delta)

;; seqR
;; New -> Old -> List
;; Takes a new element, an old element, a list, and constructs a new
;; list with the new element appended to the old element.
(defun seqR (new old l)
  (cons old (cons new l)))

(seqR 'alpha 'beta '(gamma delta)) ; (beta alpha gamma delta)

;; insert-g
;; Function -> Function
;; Takes a function and returns a function that takes a new, old
;; element and a list, and applies the function argument to insert the
;; new element before/after the old element and produces a new list.
(eval
 '(defun insert-g (seq)
    (function
     (lambda (new old l)
       (cond
	((null l) '())
	((eq (car l) old)
	 (funcall seq new old (cdr l)))
	(t (cons (car l)
		 (funcall (insert-g seq) new old (cdr l))))))))
 t)

(funcall (insert-g 'seqL) 'log 'wood '(how much wood could a wood chuck)) ; (how much log wood could a wood chuck)

(funcall (insert-g 'seqR) 'log 'wood '(how much wood could a wood chuck)) ; (how much wood log could a wood chuck)

(setq insertL (insert-g 'seqL))
(funcall insertL 'log 'wood '(how much wood could a wood chuck)) ; (how much log wood could a wood chuck)

(setq insertR (insert-g 'seqR))
(funcall insertR 'log 'wood '(how much wood could a wood chuck)) ; (how much wood log could a wood chuck)

;; 1/
;; insertL with insert-g with lambda (without seqL)

;; 2/
;; seqS

(defun seqS (new old l)
  (cons new (cdr l)))

(seqS 'log 'wood '(how much wood can a wood chuck))

;; 3/
;; subst-insert-g
(defun subst-insert-g (insert-g seqS))

;; 4/
;; subst with insert-g

;; 5/
;; rember-yyy
;; segrem

;; 6/
;; atom-to-function

;; 7/
;; value using atom-to-function

;; 8/
;; multirember-f

;; 9/
;; multirember-eq? using multirember-f

;; 10/
;; eq?-tuna
;; different way as well.

;; 11/
;; multirember T

;; 12/
;; MultiremberEco

;; 13/
;; a-friend

;; 14/
;; new-friend
;; new-friend second implementation.

;; 15/
;; new-friend (we can)

;; 16/
;; latest-friend

;; 17/
;; multiinsertLR

;; 18/
;; multiinsertLREco

;; 19/
;; multiinsertLREco dots

;; 20/
;; even?

;; 21/
;; evens-only*

;; 22/
;; evens-only*Eco

;; 23/
;; the-last-friend

;;
;; Chapter 9: ... and Again, and Again, and Again, ...
;;

;; 1/
;; looking

;; 2/
;; keep-looking

;; 3/
;; eternity

;; 4/
;; shift

;; 5/
;; align

;; 6/
;; length*

;; 7/
;; weight*

;; 8/
;; shuffle

;; 9/
;; C

;; 10/
;; A

;; 11/
;; will-stop?

;; 12/
;; last-try

;; 13/
;; length0

;; 14/
;; length-less-than-equal-to-1

;; 15/
;; length-less-than-equal-to-2

;; 16/
;; mk-length

;; 17/
;; length0 with mk-length

;; 18/
;; length-less-than-1 with mk-length

;; 19/
;; length-less-than-2 with mk-length

;; 20/
;; length-less-than-3 with mk-length

;; 21/
;; Y

;; 
;; Chapter 10: What is the value of all of this?
;;

;; 1/
;; lookup-in-entry

;; 2/
;; lookup-in-entry-help

;; 3/
;; extend-table

;; 4/
;; expression-to-action

;; 5/
;; atom-to-action

;; 6/
;; list-to-action

;; 7/
;; value-meaning

;; 8/
;; meaning

;; 9/
;; *const

;; 10/
;; *quote

;; 11/
;; text-of

;; 12/
;; *identifier

;; 13/
;; initial-table

;; 14/
;; *lambda

;; 15/
;; table-of

;; 16/
;; formals-of

;; 17/
;; body-of

;; 18/
;; evcon

;; 19/
;; else?

;; 20/
;; question-of

;; 21/
;; answer-of

;; 22/
;; *cond

;; 23/
;; cond-lines-of

;; 24/
;; evlis

;; 25/
;; *application

;; 26/
;; function-of

;; 27/
;; arguments-of

;; 28/
;; primitive?

;; 29/
;; non-primitive?

;; 30/
;; apply

;; 31/
;; apply-primitive

;; 32/
;; apply-closure


